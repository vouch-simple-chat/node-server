const { MONGODB_HOST, MONGODB_USERNAME, MONGODB_PASSWORD, MONGODB_DBNAME } = process.env

module.exports = {
    "host": MONGODB_HOST,
    "username": MONGODB_USERNAME,
    "password": MONGODB_PASSWORD,
    "database": MONGODB_DBNAME
}