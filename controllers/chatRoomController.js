const db = require("../models")

module.exports = {
    createRoom: async function (req, res) {
        //** validating req.body */

        //** parsing data from req.body */
        const { username } = req.body

        //** save a new room detail  */
        const newRoomParam = {
            username,
            message: "Hello"
        }
        await db.chats.create(newRoomParam, function (err, result) {
            if (err) {
                console.log('New room error 1: ', err);
                console.log('New room error 2: ', JSON.stringify(err));
                return res.status(500).json({
                    status: 500,
                    code: "createRoom",
                    message: "Error found went create room chat",
                    detail: err
                })
            }
            console.log('New room: ', JSON.stringify(result))
            return res.status(200).json({
                status: 200,
                code: "blogCreate",
                message: "Successfully create a new room chat",
                detail: result
            })
        })
    },
    joinRoom: async function (req, res) {
        //** validating req.body */

        //** parsing data from req.body */
        const { roomId, username } = req.body

        //** find a room using roomid */
        await db.chats.find({ roomId }, async function (err, result) {
            if (err) {
                console.log('Find room error 1: ', err);
                console.log('Find room error 2: ', JSON.stringify(err));
                return res.status(500).json({
                    status: 500,
                    code: "joinRoom",
                    message: "Error found went find room chat",
                    detail: err
                })
            }
            if (!result.length) {
                return res.status(404).json({
                    status: 404,
                    code: "joinRoom",
                    message: "Room Chat Not Found",
                    detail: err
                })
            }
            //** check username has been taken or not */
            await db.chats.find({ roomId, username }, function (err, result) {
                if (err) {
                    console.log('Find room error 3: ', err);
                    console.log('Find room error 4: ', JSON.stringify(err));
                    return res.status(500).json({
                        status: 500,
                        code: "joinRoom",
                        message: "Error found went find username in room chat",
                        detail: err
                    })
                }
                if (result) {
                    return res.status(409).json({
                        status: 409,
                        code: "joinRoom",
                        message: "Username has been taken in this room chat"
                    })
                }
                return res.status(200).json({
                    status: 200,
                    code: "joinRoom",
                    message: "Sucessfull joined the room chat",
                    detail: result
                })
            })
        })
    },
    sendMessage: async function (req, res) {
        //** validating req.body */
        //** parsing data req.body */
        const { roomId, username, message } = req.body

        //** try save message to database */
        await db.chats.find({ roomId }, async function (err, result) {
            if (err) {
                console.log('Send Message error 1: ', err);
                console.log('Send Message error 2: ', JSON.stringify(err));
                return res.status(500).json({
                    status: 500,
                    code: "sendMessage",
                    message: "Error found went find room chat",
                    detail: err
                })
            }
            if (!result) {
                return res.status(404).json({
                    status: 404,
                    code: "sendMessage",
                    message: "Room Chat Not Found",
                    detail: err
                })
            }
            await db.chats.create({ roomId, username, message }, function (err, result) {
                if (err) {
                    console.log('Send Message error 3: ', err);
                    console.log('Send Message error 4: ', JSON.stringify(err));
                    return res.status(500).json({
                        status: 500,
                        code: "sendMessage",
                        message: "Error found went try to send message to room chat",
                        detail: err
                    })
                }
                return res.status(200).json({
                    status: 200,
                    code: "sendMessage",
                    message: "Sucessfull send message to room chat",
                    detail: result
                })
            })
        })
    },
    getMessage: async function (req, res) {
        //** validating req.param */
        //** parsing data from param */
        const { roomId } = req.params
        //** try find chat from database */
        await db.chats.find({ roomId }, function (err, result) {
            if (err) {
                console.log('Get Message error 1: ', err);
                console.log('Get Message error 2: ', JSON.stringify(err));
                return res.status(500).json({
                    status: 500,
                    code: "getMessage",
                    message: "Error found went find room chat",
                    detail: err
                })
            }
            if (!result.length) {
                return res.status(200).json({
                    status: 200,
                    code: "getMessage",
                    message: "Sucessfull get message from database, but room chat is empty",
                    detail: result
                })
            }
            return res.status(200).json({
                status: 200,
                code: "getMessage",
                message: "Sucessfull get message from database",
                detail: result
            })
        })
    }
}
