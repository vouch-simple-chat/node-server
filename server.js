require('dotenv').config()
const express = require('express')
const cors = require('cors')
const fs = require('fs')

const app = express()
app.use(express.json())

//** CORS OPTION */
const corsOptionsDelegate = function (req, callback) {
    callback(null, { origin: true })
}
app.use(cors(corsOptionsDelegate))

fs.readdirSync('./routes').map(f => {
    const route = require(`./routes/${f}`)
    app.use(route)
})


const port = process.env.VOUCH_SERVER_PORT
let hostUrl = process.env.VOUCH_SERVER_HOST
hostUrl = (hostUrl.includes("localhost")) ? `${hostUrl}:${port}` : hostUrl;
app.listen(port, () => {
    console.log(`The Vouch Backend Server start and listening in ${hostUrl}`);
})
