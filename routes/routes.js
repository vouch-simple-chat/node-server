const express = require('express')
const chatRoomController = require('../controllers/chatRoomController')

const app = express.Router()

app.post('/create-room', chatRoomController.createRoom)

app.post('/join-room', chatRoomController.joinRoom)

app.post('/send-message', chatRoomController.sendMessage)

app.get('/get-message/:roomId', chatRoomController.getMessage)

module.exports = app