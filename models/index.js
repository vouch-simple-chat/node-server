const mongoose = require('mongoose')
const { customAlphabet } = require("nanoid")
const nanoid = customAlphabet('0123456789', 5)
const { host, username, password, database } = require('./config')

const option = { useNewUrlParser: true, useUnifiedTopology: true }

mongoose.connect(`mongodb+srv://${username}:${password}@${host}/${database}?retryWrites=true&w=majority`, option)

const db = {
    chats: new mongoose.model('chats', new mongoose.Schema({
        roomId: { type: String, default: nanoid() },
        username: String,
        message: String,
        timestamp: { type: Date, default: Date.now }
    }))
}

module.exports = db
